'use strict';

var connect = require('connect');
var sharejs = require('share').server;

var server = connect
(
	connect.logger(),
	connect.static(__dirname + '/../public')
);

var options = { db: { type: 'none' } };

// Attach the sharejs REST and Socket.io interfaces to the server
sharejs.attach(server, options);

var port = process.env.PORT || 5000;

server.listen
(
	port, function()
	{
		console.log('Server\'s up on port ' + port + '.');
	}
);
