var util = require('util');
var assert = require('assert');
var vows = require('vows');
var SuperMock = require('supermock').SuperMock;
var FileManager = require('../public/js/FileManager');

var Anything = SuperMock.Anything;
var Etc = SuperMock.Etc;

vows.describe('FileManager').addBatch
(
	{
		'Single topic':
		{
			topic: function ()
			{
				var scope = new SuperMock({ mockName: 'file_man.$scope' });

				return {
					scope: scope,
					unit: new FileManager(scope)
				};
			},

			'should flag its scope as ready': function (topic)
			{
				assert(topic.scope.ready);
			}
		}
	}
).export(module);