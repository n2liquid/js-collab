var module = angular.module('js-collab', []);

module.directive
(
	'codeEditor', function ()
	{
		return function ($scope, $element)
		{
			function setup_editor ()
			{
				var editor = ace.edit($element[0]);

				sharejs.open
				(
					$element.data('file-name'), 'text', '/channel',

					function (err, doc)
					{
						if (err) throw err;

						doc.attach_ace(editor);
					}
				);
			}

			$scope.$watch
			(
				'ready', function ()
				{
					if ($scope.ready)
					{
						setup_editor();
					}
				}
			);
		};
	}
);
