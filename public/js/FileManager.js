'use strict';

function FileManager ($scope)
{
	$scope.files =
	[
		{ name: 'structure.html' },
		{ name: 'style.css' },
		{ name: 'logic.js' }
	];

	$scope.ready = true;
}

if (typeof window === 'undefined')
{
	module.exports = FileManager;
}
